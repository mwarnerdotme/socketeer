# Welcome 👋

If you're looking for a small, open source project to contribute to, you're in the right place! This documentation will walk you through the basics of contributing to Socketeer.

## Getting Started

### Wiki (Documentation)

For more details on usage as well as technical overview, check out [the wiki on this repo](https://gitlab.com/mwarnerdotme/socketeer/-/wikis/home). This is the best place to go to when you have questions about how Socketeer works.

Another great place to look for documentation would be the tests in the `/src` folder. Socketeer was not built with TDD (test driven development) in mind since it's a hobby project, but **new features are built test-first.**

### Issues

All issues are kept in [the issues list for this repo](https://gitlab.com/mwarnerdotme/socketeer/-/issues). Like most repositories, there are a collection of tags as well as an issue template to help you describe your problem. Please use them! There's a guide to what the tags mean in the wiki.

## Writing code

### Branches

Master branch is protected! You will not be able to contribute code directly to the master branch. Instead, please open a new branch or fork the repository and manage your own branches. Once you're at a stopping point (i.e. feature was implemented, enhancement was made, issue was completed), send up a merge request!

### Merge requests

Please don't be discouraged if your merge request is not accepted right away - your hard work is certainly appreciated! It is important that a consistent code quality is maintained, though. Any line of code in a merge request should match the coding style in the repo and pass all requirements before it is accepted. If there are any issues with your merge request, a maintainer will let you know so that you can fix it quick and get it to a point where it is accepted.

If you have an idea that you'd like to implement into the repo, please create an issue first - then start writing code. This way there will always be a correlation between detailed high-level features/bugs and working code as solution to those problems.

### CI/CD

Gitlab Pipeline and Pages are incredible resources to have on free, open-source projects like Socketeer. Full advantage is taken of these opportunities. Check out [the pipelines file](https://gitlab.com/mwarnerdotme/socketeer/-/blob/master/.gitlab-ci.yml) for more info.

The test pipeline (for non-master branches) will automatically create draft merge requests for new branches and run the appropriate linting and test commands to make sure everything is on par. This way, all you have to do is write code, make sure it passes the checks, and open the draft when you're ready!

The master branch pipeline will run the test pipeline as well as deploy a fresh built to Pages.

### Linting

Definitely check out the commands available in the `package.json` file of this repository. There are several commands for running tests and viewing the coverage of those tests (thanks to Jest) as well as code linting with eslint and code formatting with prettier.

Most notably, the `yarn fix` or `npm run fix` command will run eslint and prettier in write mode, automatically making your code beautiful while you take another sip of your coffee. The test pipeline will then run these same two commands in check mode, which makes sure that you were running them in write mode.

## Questions & Banter

If you'd like to submit a quick question and an issue doesn't seem to fit well, please feel free to reach out to [@mwarnerdotme](https://gitlab.com/mwarnerdotme). I'd be more than happy to chat or answer questions on LinkedIn or anything sent to my email. If you live in the 21st century and you don't use LinkedIn or email, hit me up on Discord Hextide#5180. Be sure to introduce yourself first 🙂.
