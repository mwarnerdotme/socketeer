import { sessions } from 'stores/sessions'
import type Log from './Log'

import { nanoid } from 'nanoid'

class Session {
	public readonly id: string
	public readonly url: URL
	public nickname?: string
	public threshold: number
	public connection: WebSocket
	public logs: Log[]

	public onMessage?: (any) => void

	constructor(url: URL, nickname?: string) {
		this.id = nanoid(8)

		this.url = url
		this.connection = undefined
		this.logs = []
		this.threshold = 100

		if (nickname) {
			this.nickname = nickname
		}
	}

	// connect to the websocket stream
	connect(): void {
		// initiate connection
		this.appendLogs('Socketeer', `Connecting to ${this.url}...`)
		this.connection = new WebSocket(this.url.toString())

		this.connection.onopen = () => {
			this.appendLogs('Socketeer', `Connected to ${this.url}!`)
		}
		this.connection.onmessage = (event) => {
			if (this.onMessage) {
				this.appendLogs(this.url.host, event.data, 'inbound')
				this.onMessage(event.data)
			}
		}
		this.connection.onclose = () => {
			this.appendLogs('Socketeer', `Disconnected from ${this.url}.`)
		}
		this.connection.onerror = (err) => {
			this.appendLogs('Socketeer', `Could not connect to the URL ${this.url}. ${err}`)
			console.error(err)
		}
	}

	// disconnect from the websocket stream
	disconnect(): void {
		this.connection.close()
	}

	// send a websocket message
	send(message: string): void {
		try {
			// check if valid json
			const parsedMessage: Record<string, unknown> = JSON.parse(message)
			// send the message
			this.connection.send(message)
			// add to logs
			this.appendLogs('You', JSON.stringify(parsedMessage, null, 2), 'outbound')
		} catch (err) {
			console.error(err)
		}
	}

	getTabDisplay(): string {
		if (this.nickname) return this.nickname
		return `${this.url.host}${this.url.pathname}`
	}

	private appendLogs(author: string, message: string, direction = 'inbound'): void {
		const log: Log = { author, message, direction, timestamp: new Date() }
		this.logs = [...this.logs, log]
		sessions.update((s) => s)
	}
}

export default Session
