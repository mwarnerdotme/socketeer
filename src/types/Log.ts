type Log = {
	author: string
	message: string
	timestamp: Date
	direction: string
}

export default Log
