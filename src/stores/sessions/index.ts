import { writable } from 'svelte/store'
import Session from 'types/Session'

export const current = writable<string | undefined>(undefined)
export const sessions = writable<Map<string, Session>>(new Map())

export function addSession(url: URL, nickname?: string): string {
	const session = new Session(url, nickname)

	sessions.update((s) => {
		return new Map([...s.entries(), [session.id, session]])
	})

	current.set(session.id)

	return session.id
}

export function deleteSession(id: string): void {
	sessions.update((s) => {
		if (s.has(id)) {
			const success = s.delete(id)
			if (success) return s
			else throw new Error('could not delete session')
		}
	})
}
