# Socketeer 🧦 - A Browser-Based Websocket Client

We have a client for every type of server under the sun. Leveraging modern browsers, we can have a nice compact websocket client, too - for whenever you're up hacking at 3am and you need to test your streams.

Given a url to connect, Socketeer can help you subscribe to any websocket stream and send JSON messages to the other side.

![Socketeer Screenshot](https://i.imgur.com/lcdFiH2.png)

> Caution: Extremely fast and fully open source ⚡

## Live Build ☁️

Check out [Socketeer](https://mwarnerdotme.gitlab.io/socketeer/) live on the web!

## Getting Started 🥾

### Running locally

```
git clone ...
yarn
yarn dev
```

Then visit [http://localhost:5000](http://localhost:5000) in your browser.

### Building it yourself

```
git clone ...
yarn
yarn dev
```

## Built On 🧰

- [Svelte](https://github.com/sveltejs/svelte) (typescript)
- [Ace](https://ace.c9.io/#nav=about)
- [svelte-ace](https://github.com/nateshmbhat/svelte-ace)
- [LineIcons](https://lineicons.com/)
