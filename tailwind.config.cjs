const colors = require('tailwindcss/colors')
const defaultTheme = require('tailwindcss/defaultTheme')

const config = {
	mode: 'jit',
	purge: ['./src/**/*.{html,js,svelte,ts}', './dev/components/**/*.svelte'],
	darkMode: false, // or 'media' or 'class'
	theme: {
		fontFamily: {
			sans: ['Roboto', 'ui-sans-serif', 'system-ui', 'sans-serif'],
			serif: ['ui-serif', 'Georgia', 'serif'],
			mono: ['Fira Mono', 'ui-monospace', 'SFMono-Regular', 'monospace'],
		},
		colors: {
			white: colors.white,
			black: colors.black,
			gray: colors.gray,
			red: colors.red,
			orange: colors.orange,
			yellow: colors.yellow,
			green: colors.green,
			blue: colors.blue,
			indigo: colors.indigo,
			purple: colors.purple,
			violet: colors.violet,
		},
		screens: {
			xs: '320px',
			...defaultTheme.screens,
		},
		extend: {},
	},
	variants: {
		extend: {},
	},
	plugins: [
		({ addComponents, theme }) => {
			addComponents({
				'.container': {
					marginInline: 'auto',
					paddingInline: theme('spacing.4'),
					maxWidth: theme('screens.sm'),

					// Breakpoints
					'@screen sm': {
						maxWidth: theme('screens.sm'),
					},
					'@screen md': {
						maxWidth: theme('screens.md'),
					},
					'@screen lg': {
						maxWidth: theme('screens.lg'),
					},
					'@screen xl': {
						maxWidth: theme('screens.xl'),
					},
				},
			})
		},
	],
}

module.exports = config
