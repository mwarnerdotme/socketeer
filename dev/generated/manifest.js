// const d = decodeURIComponent

const templates = {
	layouts: {
		main: () => import('routes/__layout.svelte'),
	},

	errors: {
		main: () => import('../components/error.svelte'),
	},
}

const views = {
	app: {
		uri: /^\/$/,
		layout: templates.main,
		error: templates.error,
		component: () => import('routes/index.svelte'),
	},
	about: {
		uri: /^\/about\/?$/,
		layout: templates.main,
		error: templates.error,
		component: () => import('routes/about.svelte'),
	},
}

const v = views // alias to shorten calls

export const routes = [
	/* / */
	[v.app.uri, [v.app.layout, v.app.component], [v.app.error]],

	/* /about */
	[v.about.uri, [v.about.layout, v.about.component], [v.about.error]],
]

export const fallback = [templates.layouts.main(), templates.errors.main()]
